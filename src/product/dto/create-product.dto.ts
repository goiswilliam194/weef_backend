import { IsString } from 'class-validator';

export class CreateProductDto {
  @IsString()
  title: string;

  @IsString()
  value: string;

  @IsString()
  description: string;

  @IsString()
  quantity: string;

  @IsString()
  user_id: string;
}
