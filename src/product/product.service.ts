import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class ProductService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(input: CreateProductDto, image_url: string) {
    return await this.prismaService.product.create({
      data: {
        title: input.title,
        value: +input.value,
        description: input.description,
        quantity: +input.quantity,
        image_url: `http://localhost:8000/${image_url}`,
        user: {
          connect: {
            id: input.user_id,
          },
        },
      },
    });
  }

  async findAll() {
    return await this.prismaService.product.findMany();
  }

  async findOne(id: string) {
    return await this.prismaService.product.findUnique({
      where: {
        id,
      },
    });
  }

  async update(id: string, updateProductDto: UpdateProductDto) {
    return await this.prismaService.product.update({
      where: {
        id,
      },
      data: updateProductDto,
    });
  }

  async remove(id: string) {
    await this.prismaService.product.delete({
      where: {
        id,
      },
    });
  }
}
