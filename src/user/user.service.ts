import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { hash, verify } from 'argon2';

import { PrismaService } from '../database/prisma.service';

@Injectable()
export class UserService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(createUserDto: CreateUserDto) {
    const pass = await hash(createUserDto.password);

    return await this.prismaService.user.create({
      data: {
        ...createUserDto,
        password: pass,
      },
      select: {
        email: true,
        username: true,
        id: true,
      },
    });
  }

  async findOne(id: string) {
    return await this.prismaService.user.findFirst({ where: { id } });
  }

  async verifyUser(email: string, pass: string) {
    const user = await this.prismaService.user.findFirst({
      where: { email: email },
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const match = await verify(user.password, pass);

    if (!match) {
      throw new UnauthorizedException('Invalid password');
    }

    const { password, ...rest } = user;

    return rest;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    await this.prismaService.user.update({
      where: { id },
      data: updateUserDto,
    });
  }

  async remove(id: string) {
    await this.prismaService.user.delete({
      where: { id },
    });
  }
}
