import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'node:path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', '..', 'upload'), {
    prefix: '/uploads',
  });
  app.enableCors({ origin: '*' });
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(8000);
}
bootstrap();
