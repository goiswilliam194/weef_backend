/*
  Warnings:

  - You are about to drop the column `valeu` on the `products` table. All the data in the column will be lost.
  - Added the required column `value` to the `products` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "products" DROP COLUMN "valeu",
ADD COLUMN     "value" INTEGER NOT NULL;
