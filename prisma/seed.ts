import { PrismaClient } from '@prisma/client';
import { faker } from '@faker-js/faker';

const prisma = new PrismaClient();
async function main() {
  const data = Array.from({ length: 12 }).map(() => {
    return {
      title: faker.lorem.words(),
      description: faker.lorem.paragraphs(),
      value: faker.number.int({ max: 1000 }),
      quantity: faker.number.int({ max: 1000 }),
      image_url: faker.image.urlPicsumPhotos(),
    };
  });

  await prisma.product.createMany({
    data: data,
  });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
