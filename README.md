# Para rodar o projeto em sua maquina

## Instale o nodejs

[Link para o site oficial](https://nodejs.org/en)
<br/>
Ou utilize o nvm <br />
[Link para o site](https://github.com/nvm-sh/nvm)

## Instale o docker na maquina

[Link do site oficial](https://www.docker.com/)

## Verifique se o docker está instalado em sua maquina

```shell
#rode o seguinte comando

docker ps
```

Se não ocorrer nenhum erro execute

```shell
docker compose up -d
# ou
docker compose up

```

Este comando irá executar o banco de dados postgres

## Instale as dependencias utilizando o gerenciador de pacotes de sua preferencia

```shell

npm i
# ou
yarn
# ou
pnpm i
```

## Crie um arquivo .env na raiz do projeto

Copie as variaveis .env.example para o .env

## Apos instalar gere uma migration no banco de dados

```shell
npx prisma migrate dev -name "<nome>"

#logo gere a seed

npx prisma db seed
```

## Para rodar o projeto execute o seguinte comando

```shell
pnpm dev
```
